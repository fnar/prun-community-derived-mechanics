---
title: "Planet"
---

## Retrieving Raw Values

- Inspect the element in the browser (Right-click -> Inspect)
- Use FIO API to retrieve the values
   - e.g. https://fio.fnar.net/planet/Katoa


## Fertility
`Fertility = RawFertility * (10/33)`


## Resource Extraction
These values assume 100% worker efficiency.

- **Gaseous (COL):** `DailyExtraction = (RawConcentration * 100) * 0.6`
- **Other (RIG/EXT):** `DailyExtraction = (RawConcentration * 100) * 0.7`


## Resource Extraction Cycle Runtime
Each extraction building has a different base runtime:
- RIG: 4h48m (4.8h)
- COL: 6h
- EXT: 12h

Given a daily extraction value above, determine how many units per cycle.  Example of Katoa & Etherwind:

- Katoa - 16.8 Oxygen/day: `16.8 / (24h/6h)` = `4.2 O per 6h`
- Etherwind - 27.0 Oxygen/day: `27.0 / (24h/6h)` = `6.75 O per 6h`

Now, "round up" the time to the nearest unit. Take the remainder and bring the time up proportionally.

Katoa: Since we're 0.8 away from 5.0, we need to add 0.8 units worth of time.  Since a single cycle is 4.2 O, we do: `6*(0.8/4.2)`, and add that time:
`6h + (6h*(0.8/4.2))` = `5 O per 7.143h`

Etherwind: Since we're 0.25 away from 7.0, we need to add 0.25 units worth of time.  Since a single cycle is 6.72 O, we do: `6*(0.25/6.75)`, and add that time:
`6h + (6h*(0.25/6.75))` = `7 O per 6.222h`

The above assumes 100% efficiency. For other efficiencies, you modify the **calculated cycle time** appropriately.  So, if you were @ 150% efficiency, 7.143h becomes `7.143/1.5` or  `5 O per 4.762h`.